﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterAfterLogin.master" AutoEventWireup="true" CodeFile="QCGroupBookingReport.aspx.cs" Inherits="GroupSearch_Exec_QCGroupBookingReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ Register Src="~/UserControl/LeftMenu.ascx" TagPrefix="uc1" TagName="LeftMenu" %>
    <script src="http://code.jquery.com/jquery-1.8.2.js" type="text/javascript"></script>
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>" rel="stylesheet" />
    <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function openPopup(Remarks) {
            $('#lbl_Remarks').text(Remarks);
            $("#popupdiv").dialog({
                title: "Remarks",
                width: 400,
                height: 350,
                modal: true,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
        function CloseAndRefresh() {
            open(location, '_self').close();
            window.open("ExecRequestDetails.aspx");
        }
    </script>
    <script type="text/javascript">
        $(".loader").click(function (e) {
            $("#waitMessage").show();
        });
    </script>
    <script type="text/javascript">
        function loadimg() {
            $("#waitMessage").show();
        }
    </script>
    <style>
        .tooltip {
            position: relative;
            display: inline-block;
            border-bottom: 1px dotted black;
        }

            .tooltip .tooltiptext {
                visibility: hidden;
                width: 220px;
                background-color: #ff0000;
                color: #fff;
                text-align: center;
                border-radius: 6px;
                padding: 5px 0;
                /* Position the tooltip */
                position: absolute;
                z-index: 1;
            }

            .tooltip:hover .tooltiptext {
                visibility: visible;
            }
    </style>

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-10">
            <div class="page-wrapperss">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Group Booking Lookup </h3>
                    </div>
                    <div class="panel-body">


                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Date To:</label>
                                    <asp:TextBox ID="txt_todate" class="form-control date" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Date From :</label>
                                    <asp:TextBox ID="txt_fromDate" runat="server" class="form-control date"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Request Id:</label>
                                    <asp:TextBox ID="txt_RequestID" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Status:</label>
                                    <asp:DropDownList ID="ddl_status" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Value="N">---All ---</asp:ListItem>
                                        <asp:ListItem>Requested</asp:ListItem>
                                        <asp:ListItem>Freezed</asp:ListItem>
                                        <asp:ListItem>Paid</asp:ListItem>
                                        <asp:ListItem>Ticketed</asp:ListItem>
                                        <asp:ListItem>Cancelled</asp:ListItem>
                                        <asp:ListItem>Rejected</asp:ListItem>
                                        <asp:ListItem>Cancellation Requested</asp:ListItem>
                                        <asp:ListItem>Refunded</asp:ListItem>
                                        <asp:ListItem>Quoted</asp:ListItem>
                                        <asp:ListItem>Accepted</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <br />
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btn_submit" runat="server" Text="Search" CssClass="button buttonBlue" OnClick="btn_submit_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row" id="divReport" style="background-color: #fff; overflow-y: scroll; overflow-x: scroll; max-height: 500px;" runat="server">
                            <div>
                                <asp:GridView ID="gvEmployee" runat="server" AutoGenerateColumns="false" PageSize="50" AllowPaging="true" OnPageIndexChanging="gvEmployee_PageIndexChanging" CssClass="table">
                                    <HeaderStyle BackColor="#3E3E3E" Font-Names="Calibri" ForeColor="White" />
                                    <RowStyle Font-Names="Calibri" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="RequestId">
                                            <ItemTemplate>
                                                <a href="#" onclick='openWindow("<%# Eval("RequestId") %>");'>
                                                    <asp:Label ID="lblRequestID" runat="server" Text='<%#Eval("RequestId")%>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="BookingName" HeaderText="Booking Name" />
                                        <asp:BoundField DataField="Trip" HeaderText="Trip" />
                                        <asp:BoundField DataField="TripType" HeaderText="Trip Type" />
                                        <asp:BoundField DataField="NoOfPax" HeaderText="Total Pax" />
                                        <asp:BoundField DataField="ExpactedPrice" HeaderText="Expacted Price" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" />
                                        <asp:BoundField DataField="PaymentStatus" HeaderText="Payment Status" />
                                        <asp:BoundField DataField="BookedPrice" HeaderText="Booking Price" />
                                        <asp:BoundField DataField="PaymentMode" HeaderText="Payment Mode" />
                                        <asp:BoundField DataField="PgCharges" HeaderText="Pg Charges" />
                                        <asp:TemplateField>
                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                            <ItemTemplate>
                                                <div class="tooltip">
                                                    Activity
  <span class="tooltiptext">Requested By: <%# Eval("CreatedBy")%>
      <br />
      Requested Date: <%# Eval("CreatedDate")%>
      <br />
      Accepted By: <%# Eval("AcceptBy")%>
      <br />
      Accepted Date: <%# Eval("AcceptDate")%>
      <br />
      Rejected By: <%# Eval("RejectBy")%>
      <br />
      RejectDate Date: <%# Eval("AcceptDate")%>
      <br />
      Quoted By: <%# Eval("QuotedBy")%>
      <br />
      Quoted Date: <%# Eval("QuotedDate")%>
      <br />
      Freezed By: <%# Eval("FreezedBy")%>
      <br />
      Freezed Date: <%# Eval("FreezedDate")%>
      <br />
      Payement By: <%# Eval("PayementBy")%>
      <br />
      Payement Date: <%# Eval("PayementDate")%>
      <br />
      Ticketed By: <%# Eval("TicketedBy")%>
      <br />
      Ticketed Date: <%# Eval("TicketedDate")%>
      <br />
      Cancelled By: <%# Eval("CancelledBy")%>
      <br />
      Cancelled Date: <%# Eval("CancelledDate")%>
      <br />
  </span>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                        </div>
                        <div id="waitMessage" style="display: none;">
                            <div class="" style="text-align: center; opacity: 0.9; position: fixed; z-index: 99999; top: 0px; width: 100%; height: 100%; background-color: #afafaf; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; /* border: 5px solid #d1d1d1; */ /* border-radius: 10px; */">
                                <div style="position: absolute; top: 264px; left: 45%; font-size: 18px; color: #fff;">
                                    Please wait....<br />
                                    <br />
                                    <img alt="loading" src="<%=ResolveUrl("~/images/loadingAnim.gif")%>" />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="popupdiv" title="Basic modal dialog" style="display: none">
        Remarks:
            <label id="lbl_Remarks"></label>
    </div>
    <link href="http://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        function openPopup(Remarks) {
            $('#lbl_Remarks').text(Remarks);
            $("#popupdiv").dialog({
                title: "Remarks",
                width: 400,
                height: 350,
                modal: true,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
    </script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script type="text/javascript">
        $(function () {
            $(".date").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
    <script type="text/javascript">
        function openWindow(Requestid) {
            window.open('../GroupRequestidDetails.aspx?RequestID=' + Requestid, 'open_window', ' width=1024, height=720, left=0, top=0,status=yes,toolbar=no,scrollbars=yes');
        }
    </script>
    <script type="text/javascript">
        function openPopup(Remarks) {
            $('#lbl_Remarks').text(Remarks);
            $("#popupdiv").dialog({
                title: "Remarks",
                width: 400,
                height: 350,
                modal: true,
                buttons: {
                    Close: function () {
                        $(this).dialog('close');
                    }
                }
            });
        }
        function MyFunc(strmsg) {
            switch (strmsg) {
                case 1: {
                    alert("Remark can not be blank,Please Fill Remark");
                    $("#waitMessage").hide();
                }
                    break;
            }
        }
    </script>
</asp:Content>

