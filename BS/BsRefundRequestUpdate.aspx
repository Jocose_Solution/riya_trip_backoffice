﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BsRefundRequestUpdate.aspx.cs" Inherits="BS_BsRefundRequestUpdate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../css/transtour.css" rel="stylesheet" type="text/css" />
    <link href="../../css/core_style.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/lytebox.css" rel="stylesheet" type="text/css" />
    <script src="../../JS/lytebox.js" type="text/javascript"></script>
    <link href="../../CSS/itz.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/jquery-1.4.4.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) {
                return true;
            }
            else {
                return false;
            }
        }
        //$(document).ready(function () {
        //    $('#CheckBox1').change(function () {
        //        if ($(this).is(':checked')) {
        //            $("#txtid1").hide();
        //            $("#txtid2").show();

        //        }
        //        else {
        //            $("#txtid1").show();
        //            $("#txtid2").hide();
        //        }
        //    });

        //});
        function Validate() {

            if (document.getElementById("<%=txt_charge.ClientID%>").value == "" && $('#CheckBox1').is(':checked') == false) {
                alert("Please Provide Refund Charge");
                document.getElementById("<%=txt_charge.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=txt_Service.ClientID%>").value == "" && $('#CheckBox1').is(':checked') == false) {
                alert("Please Provide Service Charge");
                document.getElementById("<%=txt_Service.ClientID%>").focus();
                return false;
            }

            if (document.getElementById("<%=txtRemark.ClientID%>").value == "") {
                alert("Please Provide Remark");
                document.getElementById("<%=txtRemark.ClientID%>").focus();
                return false;
            }
            if (confirm("Are you sure!")) {
                document.getElementById("div_Submit").style.display = "none";
                document.getElementById("div_Progress").style.display = "block";
                return true;

            }
            else {
                return false;
            }
        }
    </script>



    <style type="text/css">
        #div_Submit {
            height: 36px;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnRefundid" runat="server" />
        <div class="divUpdate w100">
            <table border="1" cellpadding="0" cellspacing="0" width="100%" style="background: #fff;">
                <tr>
                    <td>
                        <h2 style="color: #000">Update Refund</h2>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" class="boxshadow w100">
                            <tr>
                                <td align="left" style="color: #fff; font-weight: bold; padding-top: 7px;" colspan="4">Agent Detail
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="clear1"></td>
                            </tr>
                            <tr>
                                <td class="bld " width="110px" height="20px" align="left">Agent ID:
                                </td>
                                <td id="td_AgentID" runat="server" class="Text" width="110px"></td>
                                <td class="bld " width="150px" height="20px" align="left">Available Credit Limit:
                                </td>
                                <td id="td_CardLimit" runat="server" class="Text"></td>
                            </tr>


                            <tr style="display: none">
                                <td class="bld " width="110px" height="20px" align="left">Agent:
                                </td>
                                <td id="td1_AgentID" runat="server" class="Text" width="110px"></td>
                                <td class="bld " width="110px" height="20px" align="left">Order ID:
                                </td>
                                <td id="td_orderid" runat="server" class="Text" width="110px"></td>
                                <td class="bld " width="110px" height="20px" align="left">seat no:
                                </td>
                                <td id="td_seat" runat="server" class="Text" width="110px"></td>
                                <td class="bld " width="110px" height="20px" align="left">Cancellation ID:
                                </td>
                                <td id="td_CancellationId" runat="server" class="Text" width="110px"></td>
                                 <td class="bld " width="110px" height="20px" align="left">TotalFare:
                                </td>
                                <td id="td_totalfare" runat="server" class="Text" width="110px"></td>
                            </tr>
                            <tr>
                                <td class="bld " width="110px" height="20px" align="left">Agent Name:
                                </td>
                                <td id="td_AgentName" runat="server" class="Text" width="110px"></td>
                                <td class="bld " align="left" width="110px">Address:
                                </td>
                                <td id="td_AgentAddress" runat="server" class="Text" width="191px"></td>
                            </tr>
                            <tr>

                                <td class="bld " align="left">Mobile No:
                                </td>
                                <td id="td_AgentMobNo" runat="server" class="Text"></td>
                            </tr>
                            <tr>
                                <td class="bld " width="110px" height="20px" align="left">Email:
                                </td>
                                <td id="td_Email" runat="server" class="Text"></td>

                            </tr>
                            <tr>
                                <asp:Label ID="lblCancellationID" runat="server" Text='<%#Eval("CANCELLATIONID")%>'></asp:Label>
                            </tr>


                               <tr>
                                <td colspan="4">
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <asp:GridView ID="grd_Pax" runat="server" AutoGenerateColumns="False" CssClass="GridViewStyle">
                                                    <Columns>
                                                        <%--<asp:TemplateField HeaderText="Pax ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPaxID" runat="server" Text='<%#Eval("PaxId") %>'></asp:Label>
                                                                <asp:HiddenField ID="HiddenMgtFee" runat="server" Value='<%#Eval("MgtFee") %>' />
                                                                <asp:HiddenField ID="HiddenSrvTax" runat="server" Value='<%#Eval("Service_Tax") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                       
                                                        <asp:TemplateField HeaderText="PAXNAME">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPAXNAME" runat="server" Text='<%#Eval("PAXNAME") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Order ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblorderId" runat="server" Text='<%#Eval("ORDERID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="PNR">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPNR" runat="server" Text='<%#Eval("PNR") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Agency Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblagencyname" runat="server" Text='<%#Eval("AGENTID") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="REFUND_AMT">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblREFUND_AMT" runat="server" Text='<%#Eval("REFUND_AMT") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="REFUND_SERVICECHRG">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblREFUND_SERVICECHRG" runat="server" Text='<%#Eval("REFUND_SERVICECHRG") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="CancelCharge">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCancelCharge" runat="server" Text='<%#Eval("CancelCharge") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="TotalFare">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalFare" runat="server" Text='<%#Eval("TA_NET_FARE") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="REFUND_STATUS">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblREFUND_STATUS" runat="server" Text='<%#Eval("REFUND_STATUS") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="CREATED_DATE">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCREATED_DATE" runat="server" Text='<%#Eval("CREATED_DATE") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                       
                                                    </Columns>
                                                    <RowStyle CssClass="RowStyle" />
                                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                                    <PagerStyle CssClass="PagerStyle" />
                                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                                    <HeaderStyle CssClass="HeaderStyle" />
                                                    <EditRowStyle CssClass="EditRowStyle" />
                                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="clear1"></td>
                            </tr>
                           
                            
                            <tr>
                                <td colspan="4">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr id="txtid1">

                                            <td width="130px" align="right">
                                                <b>Refund Charge :</b></td>
                                            <td width="20%">
                                                <asp:TextBox ID="txt_charge" runat="server" Height="29px" onkeypress="return isNumberKey(event)" MaxLength="6"></asp:TextBox>
                                            </td>


                                            <td width="130px" align="right">
                                                <b>Service Charge :</b></td>
                                            <td width="20%">
                                                <asp:TextBox ID="txt_Service" runat="server" Height="29px" onkeypress="return isNumberKey(event)" MaxLength="6"></asp:TextBox>
                                            </td>


                                            <td width="130px" align="right">
                                                <b>Remark : </b>
                                            </td>
                                            <td width="20%">
                                                <asp:TextBox ID="txtRemark" runat="server" Width="200px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                            &nbsp;&nbsp;
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="clear1"></td>
                                        </tr>

                                        <tr>
                                            <td colspan="6" align="center">
                                                <div id="div_Progress" style="display: none">
                                                    <b>Refund In Progress.</b> Please do not 'refresh' or 'back' button
                                                    <img alt="Booking In Progress" src="<%= ResolveUrl("~/images/loading_bar.gif")%>" />
                                                </div>
                                                <div id="div_Submit">
                                                    <asp:Button ID="btn_Update" runat="server" OnClick="btn_result_Click" Text="Update" OnClientClick="return Validate();" CssClass="button rgt" Width="150px" />
                                                    &nbsp;&nbsp;
                                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button rgt" OnClick="btn_Close_Click" Width="150px" />
                                                    &nbsp;&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6" class="clear"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <asp:Label ID="lblmessage" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Label ID="lbluserid" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="clear1"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
