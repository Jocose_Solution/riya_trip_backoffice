﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BusSearch.ascx.cs" Inherits="BS_UserControl_BusSearch" %>
<link href="<%=ResolveUrl("~/BS/CSS/CommonCss.css")%>" rel="stylesheet" type="text/css" />
<link href="<%=ResolveUrl("~/BS/CSS/home.css")%>" rel="stylesheet" type="text/css" />
<link href="<%=ResolveUrl("~/BS/CSS/jquery-ui-1.8.8.custom.css")%>" rel="stylesheet"
    type="text/css" />
<link href="<%=ResolveUrl("~/BS/CSS/jquery.autocomplete.css")%>" rel="stylesheet"
    type="text/css" />
<div style="width: 45%; margin: auto; padding: 10px; border: 1px solid #ccc; text-align: left;">
    <div style="line-height: 30px; font-weight: bold; font-size: 16px; color: #888; padding-left: 20px; border-bottom: 2px solid #ccc;">
        Book Bus Ticket
    </div>
    <table style="width: 100%;" cellspacing="10">
        <tr>
            <td class="fontStyle">Source
            </td>
            <td class="fontStyle">Destination
            </td>
        </tr>
        <tr>
            <td>
                <input type="text" id="txtsrc" name="txtsrc" class="txtBoxbus" />
                <input type="hidden" id="txthidsrc" name="txthidsrc" />
            </td>
            <td>
                <input type="text" id="txtdest" name="txtdest" class="txtBoxbus" />
                <input type="hidden" id="txthiddest" name="txthiddest" />
            </td>
        </tr>
        <tr>
            <td>
                <div class="divERR" id="divtxtsrc">
                    Enter source city
                </div>
            </td>
            <td>
                <div class="divERR" id="divtxtdest">
                    Enter destination city
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="height: 5px;"></td>
        </tr>
        <tr>
            <td class="fontStyle">Journey Date
            </td>
            <td class="fontStyle">
                <div style="width: 50%; float: left;">
                    Passgenger
                </div>
                <div style="width: 50%; float: left;">
                    Seat Type:
                </div>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <div>
                    <span id="currDate"></span>
                    <table class="datedt" border="0" cellpadding="0" cellspacing="5">
                        <tr>
                            <td>
                                <div id="date" class="date">
                                </div>
                            </td>
                            <td>
                                <div id="day" class="day">
                                </div>
                                <div id="month" class="mon">
                                </div>
                            </td>
                            <td>
                                <div id="year" class="year">
                                </div>
                            </td>
                            <td>
                                <input type="hidden" id="hiddepart" name="hiddepart" value="" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="float: left;">
                    <div class="divERR" id="divtxtdate">
                        Enter departure date
                    </div>
                </div>
            </td>
            <td>
                <div style="width: 50%; float: left;">
                    <div style="padding-left: 5px">
                        <select name="ddlpax" id="ddlpax" class="ddlBoxpax">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                        </select>
                    </div>
                    <div style="float: left;">
                        <div class="divERR" id="div1">
                            Select Passenger
                        </div>
                    </div>
                </div>
                <div style="width: 50%; float: left;">
                    <div style="padding-left: 5px">
                        <select name="ddlseat" id="ddlseat" class="ddlBoxbus">
                            <option value="">--Select--</option>
                            <option value="seat">Seat</option>
                            <option value="sleeper">Sleeper</option>
                        </select>
                    </div>
                    <div style="float: left;">
                        <div class="divERR" id="divddlseat">
                            Select Seat Type
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right">
                <input type="button" id="btnsearch" name="btnsearch" value="Search" class="button" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div id="divSrcDest" class="div">
                </div>
            </td>
        </tr>
    </table>
</div>
</div>

<script src="<%= ResolveUrl("~/BS/JS/jquery-1.9.1.js")%>" type="text/javascript"></script>

<script src="<%= ResolveUrl("~/BS/JS/jquery-1.4.4.min.js")%>" type="text/javascript"></script>

<script src="<%= ResolveUrl("~/BS/JS/jquery-ui-1.8.8.custom.min.js")%>" type="text/javascript"></script>

<script src="<%= ResolveUrl("~/BS/JS/BusSearch.js")%>" type="text/javascript"></script>

<script type="text/javascript">
    var UrlBase = '<%=ResolveUrl("~/") %>';
    var myDate = new Date();
    var selectDate = window.location.search.substring(1);
    var currDate = (myDate.getMonth() + 1) + '/' + (myDate.getDate()) + '/' + myDate.getFullYear();
    if (selectDate == "") {
        var d = new Date(currDate);
        var dayName = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
        var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        document.getElementById("currDate").value = currDate;
        $("#month").html(month[d.getMonth()].toUpperCase());
        $("#day").html(dayName[d.getDay()]);
        $("#date").html(d.getDate());
        $("#year").html(d.getFullYear());
    }
    else {
        var collection = {}; var k = 0;
        var qarray = selectDate.split('&');
        for (var i = 0; i <= qarray.length - 1; i++) {
            var splt = qarray[i].split('=');
            if (splt.length > 0) {
                for (var j = 0; j < splt.length - 1; j++) {
                    collection[k] = splt[j + 1];
                }
                k += 1;
            }
        }
        var ddd = collection[4].split('-');
        var d = new Date(ddd[1] + "/" + ddd[2] + "/" + ddd[0]);
        var dayName = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
        var month = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
        document.getElementById("currDate").value = currDate;
        $("#month").html(month[d.getMonth()].toUpperCase());
        $("#day").html(dayName[d.getDay()]);
        $("#date").html($.trim(ddd[2]));
        $("#year").html(d.getFullYear());
    }
</script>

