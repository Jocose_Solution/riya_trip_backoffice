﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BS/BSer.master" AutoEventWireup="true"
    CodeFile="BusSearch.aspx.cs" Inherits="BS_BusSearch" %>

<%@ Register Src="~/BS/UserControl/BusSearch.ascx" TagName="BusSearch" TagPrefix="UC1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div align="center">
        <UC1:BusSearch ID="search" runat="server">
        </UC1:BusSearch>
    </div>
</asp:Content>
