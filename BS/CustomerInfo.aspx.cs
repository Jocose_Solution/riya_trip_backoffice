﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
//using System.Web.UI.HtmlControls;

public partial class BS_CustomerInfo : System.Web.UI.Page
{
    BS_SHARED.SHARED shared; BS_BAL.SharedBAL sharedbal;
    EXCEPTION_LOG.ErrorLog erlog = new EXCEPTION_LOG.ErrorLog();
    DataColumn col = null; string amount = ""; decimal totfare = 0;
    string seat = ""; decimal srvChrg = 0; decimal tatotfare = 0;
    decimal tanetfare = 0; decimal tds = 0; decimal com = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        hideGs.Visible = false;
        string mytable = "";
        List<BS_SHARED.SHARED> list = new List<BS_SHARED.SHARED>();
        List<BS_SHARED.SHARED> breakupList = new List<BS_SHARED.SHARED>();
        try
        {
            if (!IsPostBack)
            {
                shared = new BS_SHARED.SHARED(); sharedbal = new BS_BAL.SharedBAL();
                if (Request.QueryString["ID"] != null || Request.QueryString["ID"] != "")
                {
                    shared.orderID = Request.QueryString["ID"].ToString();
                    list = sharedbal.getSelected_SeatDetails(shared);
                    Session["PaxList"] = list;
                    hidprovider.Value = list[0].provider_name;
                    if (list[0].provider_name == "GS")
                        pPaxId.Visible = false;
                    else
                        pPaxId.Visible = true;
                    mytable += "<table width='100%'>";
                    if (list.Count > 0)
                    {
                        mytable += "<tr>";
                        mytable += "<td>";
                        mytable += "<table width='100%'>";
                        mytable += "<tr>";
                        string[] strsrc = list[0].src.Split('(');
                        string[] strdest = list[0].dest.Split('(');
                        mytable += "<td colspan='3' style='line-height:35px; font-size:16px; color:#004b91; font-weight:bold;'>" + strsrc[0] + " <img src='Images/arrow.png' />&nbsp;" + strdest[0] + "</td>";
                        mytable += "</tr>";
                        mytable += "<tr>";
                        string[] strbdpoint = list[0].boardpoint.Split('&');
                        string[] strdrpoint = list[0].droppoint.Split('&');
                        mytable += "<td> <span style='font-weight:bold;'>Boarding Point : </span> " + strbdpoint[0] + "</td>";
                        mytable += "<td></td>";
                        if (strdrpoint[0].ToString() != "Not available")
                        {
                            mytable += "<td> <span style='font-weight:bold;'>Dropping Point : </span> " + strdrpoint[0] + "</td>";
                        }
                        mytable += "<td> <span style='font-weight:bold;'>Journey Date : </span>" + list[0].journeyDate.Trim().Split('-')[2] + "-" + list[0].journeyDate.Trim().Split('-')[1] + "-" + list[0].journeyDate.Trim().Split('-')[0] + " " + strbdpoint[0].Trim().Substring(strbdpoint[0].Trim().LastIndexOf('(') + 1).Replace(')', ' ') + "</td>";
                        
                        mytable += "</tr>";
                        mytable += "<tr>";
                        if (list[0].originalfare.IndexOf(",") > 0)
                        {
                            string[] strfare = list[0].originalfare.Split(',');
                            string[] strSeat = list[0].seat.Split(',');
                            for (int a = 0; a <= strfare.Length - 1; a++)
                            {
                                if (strfare[a].ToString() != "")
                                {
                                    amount += strfare[a].Trim() + ",";
                                    totfare += Convert.ToDecimal(strfare[a].Trim());
                                    seat += strSeat[a].Trim() + ","; ;
                                }
                            }
                            amount = amount.Remove(amount.LastIndexOf(","));
                            seat = seat.Remove(seat.LastIndexOf(","));
                        }
                        else
                        {
                            amount = list[0].originalfare;
                            totfare = Convert.ToDecimal(list[0].originalfare);
                            seat = list[0].seat;
                        }
                        mytable += "</tr><tr><td colspan='3'>&nbsp;</td></tr>";
                        mytable += "</table>";
                        mytable += "</td>";
                        mytable += "</tr>";
                        mytable += "</table>";
                        divUpper.InnerHtml = mytable;
                        //--------------------------------fare breakup calculation-------------------------//

                        shared.seat = seat.Trim();
                        shared.fare = Convert.ToString(amount);
                        shared.agentID = list[0].agentID.Trim();
                        shared.provider_name = list[0].provider_name.Trim();
                        breakupList = sharedbal.getCommissionList(shared);
                        for (int i = 0; i <= breakupList.Count - 1; i++)
                        {
                            srvChrg += breakupList[i].serviceChrg;
                            tatotfare += breakupList[i].taTotFare;
                            tanetfare += breakupList[i].taNetFare;
                            tds += breakupList[i].taTds;
                            com += breakupList[i].adcomm;
                        }
                        string farebreakup = "<table cellpadding='10' cellspacing='5' style='border:1px solid #eee; width:95%; padding:2%;'>";
                        farebreakup += "<tr>";
                        farebreakup += "<td colspan='2' style='font-size:16px; line-height:40px; font-weight:bold;'>Fare Breakup</td>";
                        farebreakup += "</tr>";
                        farebreakup += "<tr>";
                        farebreakup += "<td>Fare:</td><td>" + totfare + "</td>";
                        farebreakup += "</tr>";
                        farebreakup += "<tr>";
                        farebreakup += "<td>Service Charge:</td><td>" + srvChrg + "</td>";
                        farebreakup += "</tr>";
                        farebreakup += "<tr>";
                        farebreakup += "<td><a href='#' class='brk' rel='" + com + "," + tds + "," + tanetfare + "'>Total Fare:</a></td><td>" + tatotfare + "</td>";
                        farebreakup += "</tr>";
                        farebreakup += "</table>";

                        divfarebrk.InnerHtml = farebreakup;
              
                        //---------------------------------------end------------------------------------//
                        BindPax(list);
                    }
                }
                if (list[0].idproofReq.ToUpper() == "TRUE")
                {
                    tdid1.Visible = true;
                    tdid2.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            erlog = new EXCEPTION_LOG.ErrorLog();
            erlog.writeErrorLog(ex, "CustomerInfo.aspx.cs");
        }
    }
    private void BindPax(List<BS_SHARED.SHARED> list)
    {
        DataTable paxdt = new DataTable();
        col = new DataColumn();
        col.ColumnName = "PaxTP";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "seat";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "fare";
        paxdt.Columns.Add(col);
        col = new DataColumn();
        col.ColumnName = "Originalfare";
        paxdt.Columns.Add(col);

        for (int k = 0; k <= Convert.ToInt32(list[0].NoOfPax) - 1; k++)
        {
            DataRow dr = paxdt.NewRow(); string[] strSeat = null; string[] strFare = null;
            if (list[0].seat.IndexOf(",") > 0 && list[0].fare.IndexOf(",") > 0)
            {
                strSeat = list[0].seat.Split(',');
                strFare = list[0].fare.Split(',');

                dr["PaxTP"] = "Passenger :" + (k + 1) + "";
                dr["seat"] = strSeat[k].Trim();
                dr["fare"] = strFare[k].Trim();
                paxdt.Rows.Add(dr);

            }
            else
            {
                dr["PaxTP"] = "Passenger :" + (k + 1) + "";
                dr["seat"] = list[0].seat.Trim();
                dr["fare"] = list[0].fare.Trim();
                paxdt.Rows.Add(dr);

            }

        }
        rep_Pax.DataSource = paxdt;
        rep_Pax.DataBind();
    }
    protected void btnbook_Click(object sender, EventArgs e)
    {
        shared = new BS_SHARED.SHARED(); sharedbal = new BS_BAL.SharedBAL();
        List<BS_SHARED.SHARED> inventroyList = new List<BS_SHARED.SHARED>();
        shared.paxname = new List<string>(); shared.paxage = new List<string>();
        shared.title = new List<string>(); shared.gender = new List<string>();
        shared.paxseat = new List<string>(); shared.perFare = new List<string>();
        shared.boardingId = new List<string>(); shared.boardinglocation = new List<string>();
        shared.perOriginalFare = new List<string>(); decimal crdlimit = 0;
        List<BS_SHARED.SHARED> farelist = new List<BS_SHARED.SHARED>();
        List<BS_SHARED.SHARED> finallist = new List<BS_SHARED.SHARED>();
        List<BS_SHARED.SHARED> list_Pnr = new List<BS_SHARED.SHARED>();
        inventroyList = (List<BS_SHARED.SHARED>)Session["PaxList"];
        bool flag = false;
        #region[Set property value]
        try
        {
            shared.agentID = inventroyList[0].agentID;
            shared.AgencyName = inventroyList[0].AgencyName.Trim();
            shared.provider_name = inventroyList[0].provider_name;
            foreach (RepeaterItem item in rep_Pax.Items)
            {
                DropDownList dptitle = (DropDownList)item.FindControl("dptitle");
                TextBox txtname = (TextBox)item.FindControl("txtpaxname");
                TextBox txtage = (TextBox)item.FindControl("txtpaxage");
                DropDownList dpgender = (DropDownList)item.FindControl("dpgender");
                Label lblseat = (Label)item.FindControl("lblseat");
                Label lblfare = (Label)item.FindControl("lblfare");
                shared.title.Add(dptitle.SelectedValue);
                shared.paxname.Add(txtname.Text.Trim());
                shared.paxage.Add(txtage.Text.Trim());
                shared.gender.Add(dpgender.SelectedValue);
                shared.paxseat.Add(lblseat.Text.Trim());
                shared.perFare.Add(lblfare.Text.Trim());
            }
            if (inventroyList[0].originalfare.IndexOf(",") >= 0)
            {
                string[] strfare = inventroyList[0].originalfare.Split(',');
                string[] strSeat = inventroyList[0].seat.Split(',');
                for (int f = 0; f <= strfare.Length - 1; f++)
                {
                    if (strfare[f].Trim() != "")
                    {
                        shared.fare = strfare[f].Trim();
                        shared.perOriginalFare.Add(strfare[f].Trim());
                        shared.seat = strSeat[f].Trim();
                        farelist = sharedbal.getCommissionList(shared);
                        shared.subAmt += Convert.ToDecimal(farelist[0].taNetFare.ToString().Trim());
                        finallist.Add(new BS_SHARED.SHARED { adcomm = farelist[0].adcomm, taTds = farelist[0].taTds, admrkp = farelist[0].admrkp, agmrkp = farelist[0].agmrkp, taTotFare = farelist[0].taTotFare, taNetFare = farelist[0].taNetFare });
                    }
                }
            }
            else
            {
                shared.fare = inventroyList[0].originalfare.Trim();
                shared.perOriginalFare.Add(inventroyList[0].originalfare.Trim());
                shared.seat = inventroyList[0].seat.Trim();
                farelist = sharedbal.getCommissionList(shared);
                shared.subAmt += Convert.ToDecimal(farelist[0].taNetFare.ToString().Trim());
                finallist.Add(new BS_SHARED.SHARED { adcomm = farelist[0].adcomm, taTds = farelist[0].taTds, admrkp = farelist[0].admrkp, agmrkp = farelist[0].agmrkp, taTotFare = farelist[0].taTotFare, taNetFare = farelist[0].taNetFare });
            }
            shared.orderID = inventroyList[0].orderID;         
            shared.serviceID = inventroyList[0].serviceID;
            string[] srcid = inventroyList[0].src.Split('(');
            shared.src = srcid[0].Trim();
            srcid = srcid[1].Split(')');
            string[] destid = inventroyList[0].dest.Split('(');
            shared.dest = destid[0].Trim();
            destid = destid[1].Split(')');
            shared.srcID = srcid[0].Trim();
            shared.destID = destid[0].Trim();
            shared.traveler = inventroyList[0].traveler;
            shared.ladiesSeat = inventroyList[0].ladiesSeat;
            string[] board = inventroyList[0].boardpoint.Split('&');
            string[] drop = inventroyList[0].droppoint.Split('&');
            shared.boardpoint = board[0].Trim();
            shared.droppoint = drop[0].Trim();
            string[] bpointid = board[1].Trim().Split('(');
            string[] dpointid = drop[1].Trim().Split('(');
            shared.boardpointid = bpointid[0].Trim();
            shared.droppointid = dpointid[0].Trim();
            shared.journeyDate = inventroyList[0].journeyDate;
            shared.partialCanAllowed = inventroyList[0].partialCanAllowed;
            for (int n = 0; n <= shared.paxname.Count - 1; n++)
            {
                if (shared.provider_name == "GS")
                {
                        if (flag == false)
                        {
                            shared.paxname[0] = shared.paxname[0].Trim() + ",primary";
                            shared.Isprimary = "true";
                            flag =true;
                        }
                }
                else
                {
                    if (shared.paxname[n].ToString().Trim() == Request["txtprimarypax"].ToString().Trim())
                    {
                        shared.paxname[n] = shared.paxname[n].Trim() + ",primary";
                        shared.Isprimary = "true";
                        flag = true;
                    }
                    else
                    {
                        if (flag == false)
                        {
                            shared.paxname[0] = shared.paxname[0].Trim() + ",primary";
                            shared.Isprimary = "true";
                        }
                    }
                }
            }
            shared.NoOfPax =Convert.ToString(shared.paxname.Count);
            shared.paxmob = Request["txtmob"].ToString().Trim();
            shared.paxemail = Request["txtemail"].ToString().Trim();
            shared.paxaddress = Request["txtaddress"].ToString().Trim();
            shared.provider_name = inventroyList[0].provider_name.Trim();
            shared.traveler = inventroyList[0].traveler.Trim();
            shared.canPolicy = inventroyList[0].canPolicy.Trim();
            if (inventroyList[0].idproofReq == "true")
            {
                shared.idnumber = Request["txtcard"].ToString().Trim();
                shared.idtype = Request["idproof"].ToString().Trim();
            }
            else
            {
                shared.idproofReq = "false";
            }
        #endregion

        #region[Check Credit Limit and Final Book]
            crdlimit = sharedbal.getCrdLimit(shared.agentID.Trim());
            if (crdlimit >= shared.subAmt)
            {
                //------------------------insert into database------------------------//
                if (shared.provider_name == "GS")
                {
                    shared.blockKey = sharedbal.getSelectedSeat_BlockKey(shared);
                    if (shared.blockKey.Trim() == "Error" || shared.blockKey.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alert", "alert('The seat has already been booked..please try after some time.');", true);
                    }
                    else
                    {
                        finallist = sharedbal.SetPaxInformationGS(shared);
                        if (finallist[0].orderID == "Error")
                        {
                            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "alert", "alert('please contact administrator.');", true);
                        }
                        else
                        {
                            // NAV CALL START
                            try
                            {
                              //  NaVBusService.busBooking objNavBus = new NaVBusService.busBooking(shared.orderID.Trim().ToString());
                                //objNavBus.pushBusToNav(shared.orderID.ToString().Trim(), "", "0");
                            }
                            catch (Exception ex)
                            {
                            }
                            // NAV CALL END
                            sharedbal.insertselected_seatforbook(shared, finallist);
                            ghxsgh.InnerHtml = @finallist[0].bookres;
                            ghxsgh.Visible = true;
                            hideGs.Visible = true;
                            ordrId.InnerHtml = @finallist[0].orderID;
                            ordrId.Visible = false;
                            tblhidefalse.Visible = false;
                        }
                    }
                    //doc.LoadHtml();
                }
                else if (shared.provider_name == "RB")
                {
                    ghxsgh.Visible = false;
                    tblhidefalse.Visible = true;
                    ordrId.Visible = false;
                    hideGs.Visible = false;
                    sharedbal.insertselected_seatforbook(shared, finallist);
                    shared.blockKey = sharedbal.getSelectedSeat_BlockKey(shared);
                    if (shared.blockKey.Trim() == "Error")
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('The seat has already been booked..please try after some time');", true);
                    }
                    else
                    {
                        //----------------------final book--------------------//
                        list_Pnr = sharedbal.getSelectedSeat_TicketNo(shared);
                        if (list_Pnr[0].status == "Fail" || list_Pnr[0].status == "Error")
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('" + list_Pnr[0].bookres.Trim() + "');", true);
                        }
                        else
                        {
                            // NAV CALL START
                            try
                            {
                                //NaVBusService.busBooking objNavBus = new NaVBusService.busBooking(shared.orderID.Trim().ToString());
                                //objNavBus.pushBusToNav(shared.orderID.ToString().Trim(), "", "0");
                            }
                            catch (Exception ex)
                            {
                            }
                            // NAV CALL END
                            Response.Redirect("TicketCopy.aspx?tin=" + list_Pnr[0].tin.Trim() + "&oid=" + shared.orderID.Trim() + "");
                        }
                    }
                }
                else if (shared.provider_name == "AB")
                {
                    ghxsgh.Visible = false;
                    tblhidefalse.Visible = true;
                    ordrId.Visible = false;
                    hideGs.Visible = false;
                    sharedbal.insertselected_seatforbook(shared, finallist);
                    shared.blockKey = sharedbal.getSelectedSeat_BlockKey(shared);
                    if (shared.blockKey.Trim() == "Error")
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('The seat has already been booked..please try after some time');", true);
                    }
                    else
                    {
                        //----------------------final book--------------------//
                        list_Pnr = sharedbal.getSelectedSeat_TicketNo(shared);
                        if (list_Pnr[0].status == "Fail" || list_Pnr[0].status == "Error")
                        {
                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('" + list_Pnr[0].bookres.Trim() + "');", true);
                        }
                        else
                        {
                            // NAV CALL START
                            try
                            {
                                //NaVBusService.busBooking objNavBus = new NaVBusService.busBooking(shared.orderID.Trim().ToString());
                                //objNavBus.pushBusToNav(shared.orderID.ToString().Trim(), "", "0");
                            }
                            catch (Exception ex)
                            {
                            }
                            // NAV CALL END
                            Response.Redirect("TicketCopy.aspx?tin=" + list_Pnr[0].tin.Trim() + "&oid=" + shared.orderID.Trim() + "");
                        }
                    }
                }
                
                //sharedbal.insertselected_seatforbook(shared, finallist);
                ////----------------------------end-------------------------------------//
                ////-------------------------final book here-----------------------------//
                //shared.blockKey = sharedbal.getSelectedSeat_BlockKey(shared);
              
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('you have insufficient balance');", true);
            }
            //------------------------end------------------------//
            #endregion
        }
        catch (Exception ex)
        {
            erlog = new EXCEPTION_LOG.ErrorLog();
            erlog.writeErrorLog(ex, "CustomerInfo.aspx.cs");
        }
    }

 
    protected void btnbookGs_Click(object sender, EventArgs e)
    {
        List<BS_SHARED.SHARED> list_Pnr1 = new List<BS_SHARED.SHARED>();
        BS_SHARED.SHARED shareddata = new BS_SHARED.SHARED();
        BS_BAL.SharedBAL sharedbal1=new BS_BAL.SharedBAL();
        try
        {
           
            shareddata = sharedbal1.getSelectedSeatPaxInfo("", ordrId.InnerText.Trim());
            list_Pnr1 = sharedbal1.getSelectedSeat_TicketNo(shareddata);
            if (list_Pnr1[0].status == "Fail" || list_Pnr1[0].status == "Error" || list_Pnr1[0].status == "FAIL")
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('" + list_Pnr1[0].bookres.Trim() + "');", true);
            }
            else
            {
                Response.Redirect("TicketCopy.aspx?tin=" + list_Pnr1[0].tin.Trim() + "&oid=" + shareddata.orderID.Trim() + "&provider=" + shareddata.provider_name+ "");
             
            }
        }
        catch (Exception ex)
        {
            erlog = new EXCEPTION_LOG.ErrorLog();
            erlog.writeErrorLog(ex, "CustomerInfo.aspx.cs");
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Alert", "alert('" + list_Pnr1[0].bookres.Trim() + "');", true);
        }
    }
}
