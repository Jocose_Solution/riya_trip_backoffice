﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Notification;



public partial class Notification_Form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
          if(Session["UID"] == null ||  Convert.ToString(Session["UID"]) =="")
          {
              Response.Redirect("Login.aspx");
          }
        if (!IsPostBack)
        {
            DROPBIND();
            BindEmployeeDetails();
        }
    }
    public void DROPBIND()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("Sp_UserType", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        dd_UserType.DataSource = ds;
        dd_UserType.DataTextField = "UserType";
        dd_UserType.DataValueField = "UserType";
        dd_UserType.DataBind();
        dd_UserType.Items.Insert(0, new ListItem("--Select--", "0"));
        dd_UserType.Items.Insert(1, new ListItem("ALL", "ALL"));
        con.Close();


        SqlConnection con1 = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con1.Open();
        SqlCommand cmd1 = new SqlCommand("SP_typeID", con1);
        SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
        DataSet ds1 = new DataSet();
        da1.Fill(ds1);
        DD_SPECIALType.DataSource = ds1;
        DD_SPECIALType.DataTextField = "GType";
        DD_SPECIALType.DataValueField = "GType";
        DD_SPECIALType.DataBind();
        DD_SPECIALType.Items.Insert(0, new ListItem("--Select--", ""));
        DD_SPECIALType.Items.Insert(1, new ListItem("ALL", "ALL"));
        con1.Close();




        DDStartTimeHH.Items.Clear();
        DDStartTimeHH.Items.Insert(0, new ListItem("Start Time", "0"));
        for (int i = 0; i <= 23; i++)
        {
            if (i >= 0 && i <= 9)
            {
                string a = Convert.ToString("0" + i);
                DDStartTimeHH.Items.Add(a.ToString());

            }
            else
            {
                DDStartTimeHH.Items.Add(i.ToString());


            }

        }
        DDStartTimeMM.Items.Clear();
        DDStartTimeMM.Items.Insert(0, new ListItem("Start Min", "0"));
        for (int i = 0; i <= 59; i++)
        {
            if (i >= 0 && i <= 9)
            {
                string a = Convert.ToString("0" + i);
                DDStartTimeMM.Items.Add(a.ToString());
            }
            else
            {
                DDStartTimeMM.Items.Add(i.ToString());
            }
        }

        //////////////////////////////////


        DDEndTimeHH.Items.Clear();
        DDEndTimeHH.Items.Insert(0, new ListItem("End Time", "0"));
        for (int i = 0; i <= 23; i++)
        {
            if (i >= 0 && i <= 9)
            {
                string a = Convert.ToString("0" + i);
                DDEndTimeHH.Items.Add(a.ToString());


            }
            else
            {
                DDEndTimeHH.Items.Add(i.ToString());
            }

        }
        DDEndTimeMM.Items.Clear();
        //DDEndTimeMM.Items.Add(string.Empty);
        DDEndTimeMM.Items.Insert(0, new ListItem("End Min", "0"));
        for (int i = 0; i <= 59; i++)
        {
            if (i >= 0 && i <= 9)
            {
                string a = Convert.ToString("0" + i);
                DDEndTimeMM.Items.Add(a.ToString());

            }
            else
            {
                DDEndTimeMM.Items.Add(i.ToString());
            }
        }
    }
    protected void btn_Submit_Click(object sender, EventArgs e)
    {


        try
        {
            WebNotification objwb = new WebNotification();
            string UserID = "";
            if (!string.IsNullOrEmpty(txtAgencyName.Value) && txtAgencyName.Value != "Agency Name or ID")
            {
                string[] Agentid = txtAgencyName.Value.Split('(');
                UserID = Agentid[1].Replace(")", "");

            }

            objwb.Title = txt_Tittle.Text;
            objwb.Message = txt_Message.Text;
            objwb.UserType = dd_UserType.SelectedValue;
            objwb.SpecialType = DD_SPECIALType.SelectedValue == "" ? "ALL" : DD_SPECIALType.SelectedValue;
            objwb.DisplayType = DD_DISTYPE.SelectedValue;
            objwb.UserID = UserID;
            objwb.PageName = PageName.Text;
            objwb.CreateBy = Session["UID"].ToString();
            objwb.StartDate = Convert.ToDateTime(From.Value.ToString());
            objwb.EndDate = Convert.ToDateTime(To.Value.ToString());
            objwb.starthour = DDStartTimeHH.SelectedValue;
            objwb.startmin = DDStartTimeMM.SelectedValue;
            objwb.Endhour = DDEndTimeHH.SelectedValue;
            objwb.Endmin = DDEndTimeMM.SelectedValue;
            Notify ObjN = new Notify();
            int count = ObjN.Insert_WebNotification(objwb);
            if (count > 0)
            {
                string message = "Insert Record Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                Clear();
                BindEmployeeDetails();
            }
            else
            {

                string message = "Not Insert Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                Clear();
                BindEmployeeDetails();

            }
        }
        catch (Exception ex)
        {
            string message = "Something went Wrong!! Try Again Later.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            Clear();
            BindEmployeeDetails();   
        }


    }
    public void Clear()
    {
        txt_Tittle.Text = "";
        txt_Message.Text = "";
        dd_UserType.SelectedIndex = 0;
        DD_SPECIALType.SelectedIndex = 0;
        DD_DISTYPE.SelectedIndex = 0;
        PageName.Text = "";
        txtAgencyName.Value = "";
        DDStartTimeHH.SelectedIndex = 0;
        DDStartTimeMM.SelectedIndex = 0;
        DDEndTimeHH.SelectedIndex = 0;
        DDEndTimeMM.SelectedIndex = 0;
        From.Value = "";
        To.Value = "";

    }
    protected void BindEmployeeDetails()
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("SP_FetchRecord_webnotification", con);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        con.Close();
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvDetails.DataSource = ds;
            gvDetails.DataBind();
        }
        else
        {
            ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
            gvDetails.DataSource = ds;
            gvDetails.DataBind();
            int columncount = gvDetails.Rows[0].Cells.Count;
            gvDetails.Rows[0].Cells.Clear();
            gvDetails.Rows[0].Cells.Add(new TableCell());
            gvDetails.Rows[0].Cells[0].ColumnSpan = columncount;
            gvDetails.Rows[0].Cells[0].Text = "No Records Found";
        }
    }

    protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gvDetails.EditIndex = -1;
        BindEmployeeDetails();

    }
    protected void gvDetails_RowDeleted(object sender, GridViewDeletedEventArgs e)
    {

    }
    protected void gvDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gvDetails.EditIndex = e.NewEditIndex;
        BindEmployeeDetails();
    }
    protected void gvDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int ID = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Values["ID"].ToString());

            TextBox Gtxt_tittle = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("Gtxt_tittle");
            TextBox Gtxt_message = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("Gtxt_message");
            TextBox Gtxt_StartDate = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("Gtxt_StartDate");
            TextBox Gtxt_endDate = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("Gtxt_endDate");
            TextBox Elbl_PGName = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("Elbl_PGName");
            DropDownList GGDD_DISTYPE = (DropDownList)gvDetails.Rows[e.RowIndex].FindControl("GGDD_DISTYPE");
            DropDownList GDDStartTimeHH = (DropDownList)gvDetails.Rows[e.RowIndex].FindControl("GDDStartTimeHH");
            DropDownList GDDStartTimeMM = (DropDownList)gvDetails.Rows[e.RowIndex].FindControl("GDDStartTimeMM");
            DropDownList GDDEndTimeHH = (DropDownList)gvDetails.Rows[e.RowIndex].FindControl("GDDEndTimeHH");
            DropDownList GDDEndTimeMM = (DropDownList)gvDetails.Rows[e.RowIndex].FindControl("GDDEndTimeMM");


            WebNotification OBJ = new WebNotification();
            OBJ.Title = Gtxt_tittle.Text;

            OBJ.Message = Gtxt_message.Text;
            OBJ.DisplayType = GGDD_DISTYPE.SelectedValue;
            OBJ.StartDate = Convert.ToDateTime(Gtxt_StartDate.Text);
            OBJ.EndDate = Convert.ToDateTime(Gtxt_endDate.Text);
            OBJ.starthour = GDDStartTimeHH.SelectedValue;
            OBJ.startmin = GDDStartTimeMM.SelectedValue;
            OBJ.Endhour = GDDEndTimeHH.SelectedValue;
            OBJ.Endmin = GDDEndTimeMM.SelectedValue;
            OBJ.PageName = Elbl_PGName.Text;
            OBJ.ID = ID;
            OBJ.UpdateBy = Session["UID"].ToString();

            Notify OBJNS = new Notify();
            int a = OBJNS.Update_WebNotification(OBJ);

            if (a > 0)
            {
                string message = "Update Record Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                Clear();

            }
            else
            {
                string message = "Record not be Updated Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
                Clear();
            
            }
            gvDetails.EditIndex = -1;
            BindEmployeeDetails();
        }
        catch
        {

            string message = "Something went Wrong!!Try again.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            Clear();
            gvDetails.EditIndex = -1;
            BindEmployeeDetails();
        }
    }
    protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void gvDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && gvDetails.EditIndex == e.Row.RowIndex)
        {
            DropDownList GGDD_DISTYPE = (DropDownList)e.Row.FindControl("GGDD_DISTYPE");
            GGDD_DISTYPE.DataBind();
            GGDD_DISTYPE.Items.FindByValue((e.Row.FindControl("Elbl_TypeOfmsg") as Label).Text).Selected = true;


            ////////////////////////////
            DropDownList GDDStartTimeHH = (DropDownList)e.Row.FindControl("GDDStartTimeHH");
            GDDStartTimeHH.Items.Clear();
            GDDStartTimeHH.Items.Insert(0, new ListItem("Start Hour", "0"));
            for (int i = 0; i <= 23; i++)
            {
                if (i >= 0 && i <= 9)
                {
                    string a = Convert.ToString("0" + i);
                    GDDStartTimeHH.Items.Add(a.ToString());
                }
                else
                {
                    GDDStartTimeHH.Items.Add(i.ToString());
                }

            }

            GDDStartTimeHH.DataBind();
            GDDStartTimeHH.Items.FindByValue((e.Row.FindControl("st_hours") as Label).Text).Selected = true;

            /////////////

            DropDownList GDDStartTimeMM = (DropDownList)e.Row.FindControl("GDDStartTimeMM");
            GDDStartTimeMM.Items.Clear();
            GDDStartTimeMM.Items.Insert(0, new ListItem("Start Min", "0"));
            for (int i = 0; i <= 59; i++)
            {
                if (i >= 0 && i <= 9)
                {
                    string a = Convert.ToString("0" + i);
                    GDDStartTimeMM.Items.Add(a.ToString());
                }
                else
                {
                    GDDStartTimeMM.Items.Add(i.ToString());
                }

            }

            GDDStartTimeMM.DataBind();
            GDDStartTimeMM.Items.FindByValue((e.Row.FindControl("st_min") as Label).Text).Selected = true;

            //////////////////////////////

            DropDownList GDDEndTimeHH = (DropDownList)e.Row.FindControl("GDDEndTimeHH");
            GDDEndTimeHH.Items.Clear();
            GDDEndTimeHH.Items.Insert(0, new ListItem("End Hour", "0"));
            for (int i = 0; i <= 23; i++)
            {
                if (i >= 0 && i <= 9)
                {
                    string a = Convert.ToString("0" + i);
                    GDDEndTimeHH.Items.Add(a.ToString());


                }
                else
                {
                    GDDEndTimeHH.Items.Add(i.ToString());
                }

            }

            GDDEndTimeHH.DataBind();
            GDDEndTimeHH.Items.FindByValue((e.Row.FindControl("end_hours") as Label).Text).Selected = true;

            ///////////////////////////////////

            DropDownList GDDEndTimeMM = (DropDownList)e.Row.FindControl("GDDEndTimeMM");
            GDDEndTimeMM.Items.Clear();
            GDDEndTimeMM.Items.Insert(0, new ListItem("End Min", "0"));
            for (int i = 0; i <= 59; i++)
            {
                if (i >= 0 && i <= 9)
                {
                    string a = Convert.ToString("0" + i);
                    GDDEndTimeMM.Items.Add(a.ToString());


                }
                else
                {
                    GDDEndTimeMM.Items.Add(i.ToString());
                }

            }

            GDDEndTimeMM.DataBind();
            GDDEndTimeMM.Items.FindByValue((e.Row.FindControl("end_min") as Label).Text).Selected = true;
        }


    }
    protected void gvDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int ID = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Values["ID"].ToString());
        int temp = 0;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        try
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Sp_Delete_WebnotificationByID", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ID", ID);
            temp = cmd.ExecuteNonQuery();
            con.Close();


            if (temp > 0)
            {
                BindEmployeeDetails();
                string message = "Delete Record Successfully.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
            else
            {
                BindEmployeeDetails();
                string message = "Not Deleted!! Try again.";
                string script = "window.onload = function(){ alert('";
                script += message;
                script += "')};";
                ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
            }
        }
        catch (Exception EX)
        {
            BindEmployeeDetails();
            string message = "Not Deleted!! Try again.";
            string script = "window.onload = function(){ alert('";
            script += message;
            script += "')};";
            ClientScript.RegisterStartupScript(this.GetType(), "SuccessMessage", script, true);
        }
    }
    protected void gvDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        
            gvDetails.PageIndex = e.NewPageIndex;
            BindEmployeeDetails();
    }
}