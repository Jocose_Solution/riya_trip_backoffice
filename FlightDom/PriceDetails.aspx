﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PriceDetails.aspx.vb" MasterPageFile="~/MasterAfterLogin.master"
    Inherits="FlightDom_PriceDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <link href="../CSS/astyle.css" rel="stylesheet" />--%>
    <style type="type/css">
        .f18 {
            font-size: 18px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function Validate() {
            if (confirm("Are you sure!")) {

                return true;
            }
            else {
                return false;
            }
        }
    </script>
    <div style="margin-top: 30px;"></div>
    <div style="text-align: right; width: 90%;">
        <input type="button" id="btnBookAnother" value="Book Other Flight" class="button" style="width: 150px; height: 50px;" name="<%=Session("SearchCriteriaUser").ToString()%>" />
    </div>
    <div class="large-12 medium-12 small-12 columns">
        <%--<div class="f16 bgf1 bld padding1">
            Itinerary Details</div>
        <hr />--%>
        
        <div class="large-12 medium-12 small-12" id="divFltDtls" runat="server">
        </div>

        <div class="large-12 medium-12 small-12" id="divPaxdetails" runat="server">
        </div>

        <div class="large-12 medium-12 small-12" id="divFareDtls" runat="server">
        </div>

        <div class="large-12 medium-12 small-12" id="divFareDtlsR" runat="server">
        </div>
        <div class="clear1"></div>
        <div class="large-2 medium-2 small-12 large-push-9 medium-push-9">
            <asp:Button ID="Submit" runat="server" Text="Book"  OnClientClick="return Validate()" />
        </div>

        <div class="clear">
        </div>

    </div>
    <script type="text/javascript">
        function funcnetfare(arg, id) {
            document.getElementById(id).style.display = arg

        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnBookAnother").click(function () {
                window.location.href = $.trim($(this).attr("name"))
            });
        });
    </script>
</asp:Content>
