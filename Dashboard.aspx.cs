﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
public partial class Dashboard : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
      
            BindCount();      
    }
    protected void BindCount()
    {
        try
        {
            if (Session["User_Type"].ToString() == "SALES")
            {
                Response.Redirect("SprReports/Admin/Agent_Details.aspx", false);
            }
        DataSet ds = new DataSet();
        string constr = ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
        SqlConnection con = new SqlConnection(constr);
        con.Open();

        SqlCommand cmd = new SqlCommand("usp_Get_Ticket_Satus_wise_Count");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@startDate", "2016-06-15 15:15:23.210");
        cmd.Parameters.AddWithValue("@endDate", "2016-06-15 15:15:23.210");
        SqlDataAdapter sda = new SqlDataAdapter();
        cmd.Connection = con;
        sda.SelectCommand = cmd;

        DataTable dt = new DataTable();
        sda.Fill(ds);
        dt = ds.Tables[0];

        lblTKTC.Text = Convert.ToString(dt.Rows[0]["TicketCount"].ToString());
        lblhold.Text = Convert.ToString(ds.Tables[0].Rows[0]["HoldNum"].ToString());
        lblreissueReq.Text = Convert.ToString(dt.Rows[0]["reissueReq"].ToString());
        lblCancelReq.Text = Convert.ToString(dt.Rows[0]["CancelReq"].ToString());
        lbltktreq.Text = Convert.ToString(dt.Rows[0]["TKTREQ"].ToString());
        lblprehold.Text = Convert.ToString(dt.Rows[0]["PreHold"]);
        lblHoldByAgent.Text = Convert.ToString(dt.Rows[0]["HoldByAgentFlight"]);

            lblbscount.Text = Convert.ToString(ds.Tables[0].Rows[0]["BSCOUNT"].ToString());
        lblbshold.Text = Convert.ToString(dt.Rows[0]["BSHOLD"].ToString());
        lblbscancel.Text = Convert.ToString(dt.Rows[0]["BSCANCEL"].ToString());
        lblbsrequst.Text = Convert.ToString(dt.Rows[0]["BSREQ"].ToString());


        lblhtlcount.Text = Convert.ToString(ds.Tables[0].Rows[0]["HTLCOUNT"].ToString());
        lblhtlhold.Text = Convert.ToString(dt.Rows[0]["HTLHOLD"].ToString());
        lblhtlcancel.Text = Convert.ToString(dt.Rows[0]["HTLCANCEL"].ToString());
        lblhtlrequest.Text = Convert.ToString(dt.Rows[0]["HTLREQ"].ToString());
        RailID.Text = "Count:" + Convert.ToString(dt.Rows[0]["Railcount"].ToString()); 
         RailCount.Text = "Sum:" + Convert.ToString(dt.Rows[0]["RailSumcount"].ToString());
           

   

        //DataList1.DataSource = ds.Tables[1];
        //DataList1.DataBind();

        //DataList2.DataSource = ds.Tables[2];
        //DataList2.DataBind();

        //DataList3.DataSource = ds.Tables[3];
        //DataList3.DataBind();


        //DataList4.DataSource = ds.Tables[4];
        //DataList4.DataBind();
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
        }

    }
}