﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IBEHome.aspx.vb" Inherits="IBEHome"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearch.ascx" TagName="IBESearch" TagPrefix="Search" %>
<%@ Register Src="~/UserControl/HotelSearch.ascx" TagName="HotelSearch" TagPrefix="HotelSearch" %>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder1" ID="Cont1" runat="server">

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

  
    <style type="text/css">
        .booking-remarks
        {
            display: none;
            height: auto;
            width: 30%;
            border: 6px solid #333;
            bottom: 3%;
            z-index: 1;
            position: fixed;
            left: 3%;
            padding: 0px 5px;
            background-color: #fff;
            vertical-align: top;
        }
    </style>
    <link href="Utility/css/basic.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">


        function passchange() {
            $("#passchange1").fadeIn();
        }
        function passchangeclose() {
            $("#passchange1").fadeOut();
        }
        function passchangevalidation() {
            if ($("#ctl00_ContentPlaceHolder1_oldpwd").val() == "") {
                alert('Please enter old password');
                $("#ctl00_ContentPlaceHolder1_oldpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() == "") {
                alert('Please enter new password');
                $("#ctl00_ContentPlaceHolder1_newpwd").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwdconf").val() == "") {
                alert('Please enter confirm new password');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
            if ($("#ctl00_ContentPlaceHolder1_newpwd").val() != $("#ctl00_ContentPlaceHolder1_newpwdconf").val()) {
                alert('New password and confirmation password is not matching.  ');
                $("#ctl00_ContentPlaceHolder1_newpwdconf").focus();
                return false;
            }
        }
    </script>

    <%--<div id="passchange1" style="position: fixed; top: 0; left: 0; height: 100%; width: 100%;
        background: url(images/fade.png); display: none;">
        <div style="position: relative; line-height: 25px; top: 150px; left: 38%; width: 26%;
            padding: 1%; background: #f1f1f1; border: 5px solid #d1d1d1;">
            <h1 style="color: #888; text-align: center;">
                Please change your current password</h1>
            <hr />
            <div class="clear1">
            </div>
            <div class="lft w33">
                User Id</div>--%>
            <div class="rgt" id="userid" runat="server" style="text-align: left">
            </div>
           <%--< <div class="clear1">
            </div>
            <div class="lft w33">
                Old Password</div>
            <div class="rgt">
               
                <asp:TextBox ID="oldpwd" runat="server" CssClass="padding1 brdr" TextMode="Password"></asp:TextBox>
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div class="lft w33">
                New Password</div>
            <div class="rgt">
              
                <asp:TextBox ID="newpwd" runat="server" CssClass="padding1 brdr" TextMode="Password"></asp:TextBox>
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div class="lft w36">
                Confirm New Password</div>
            <div class="rgt">
               
                <asp:TextBox ID="newpwdconf" runat="server" CssClass="padding1 brdr" TextMode="Password"></asp:TextBox>
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div class="clear1">
            </div>
            <div style="margin-left: 10%">
                <asp:Button ID="btn_resetpwd" runat="server" Text="Reset"
                    CssClass="button" /> <%--OnClientClick="return passchangevalidation();"--%>
            <%--</div>
        </div>
    </div>--%>
    <table cellspacing="0" cellpadding="0" class="w100" border="0">
        <tr>
            <td valign="top">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2">
                            <Search:IBESearch ID="IBE_CP" runat="server" />
                            <HotelSearch:HotelSearch ID="HotelSearch" runat="server" />
                        </td>
                    </tr>
                    
                    
                </table>
            </td>
            <%--<td rowspan="2" valign="top">
                <table>
                    <tr>
                        <td style="border: 1px solid #ccc;">
                            <a target="_blank" href="Adds/Offers/ViewOffers.aspx?imgname=iber1" id="ancIbehomer1">
                                <img src="" alt="" border="0" id="imgibhomer1" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc;">
                            <a target="_blank" href="Adds/Offers/ViewOffers.aspx?imgname=iber2" id="ancIbehomer2">
                                <img src="" alt="" border="0" id="imgibhomer2" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ccc;">
                            <a target="_blank" href="Adds/Offers/ViewOffers.aspx?imgname=iber3" id="ancIbehomer3">
                                <img src="" alt="" border="0" id="imgibhomer3" />
                            </a>
                        </td>
                    </tr>
                </table>
            </td>--%>
        </tr>
    </table>
    <div class="booking-remarks" id="divStockistList">
        <img id="imgCloseCHSch" src="Images/closebox.png" alt="" style="float: right; z-index: 9999;
            cursor: pointer; margin-top: -27px; margin-right: -20px;" title="Close" />
        <%--<HS:CHSearch ModuleType="rail" ID="chsFlightOffer" runat="server" />
        <SUC:Stockist ID="stockist1" runat="server" />--%>
    </div>

    <script src="Scripts/SetOffers.js" type="text/javascript"></script>

    <script type="text/javascript">

        if ('<%=Request("Htl")%>' == 'H') {
            $("#img2-").show();
            $("#img1").hide();
        }
        else {
            $("#img1").show();
            $("#img2").hide();
        }
    </script>

</asp:Content>
