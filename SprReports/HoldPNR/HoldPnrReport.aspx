﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="HoldPnrReport.aspx.vb" Inherits="SprReports_HoldPNR_HoldPnrReport" MasterPageFile="~/MasterAfterLogin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <link href="<%=ResolveUrl("~/css/lytebox.css")%>" rel="stylesheet" type="text/css" />
    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"   rel="stylesheet" />
     

    <style type="text/css">
        .page-wrapperss {
            background-color: #fff;
            margin-left: 15px;
        }

        .overfl {
            overflow: auto;
        }
    </style>

    <div class="row">
        <div class="col-md-12"  >
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Flight > Search Flight Hold Pnrs </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">From Date</label>
                                    <input type="text" name="From" id="From" class="form-control" readonly="readonly" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">To Date</label>
                                <input type="text" name="To" id="To" class="form-control" readonly="readonly" />
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">PNR</label>
                                <asp:TextBox ID="txt_PNR" class="form-control" runat="server"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Order Id</label>
                                    <asp:TextBox ID="txt_OrderId" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Pax Name</label>
                                <asp:TextBox ID="txt_PaxName" class="form-control" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-4">
                                <label for="exampleInputPassword1">Airline</label>
                                <asp:TextBox ID="txt_AirPNR" class="form-control" runat="server"></asp:TextBox>
                            </div>


                        </div>

                        <div id="td_Agency" runat="server">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Exec ID</label>
                                        <asp:DropDownList ID="ddl_ExecID" class="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <label for="exampleInputPassword1">Agency</label>
                                    <input type="text" id="txtAgencyName" class="form-control" name="txtAgencyName" onfocus="focusObjag(this);"
                                        onblur="blurObjag(this);" defvalue="ALL" autocomplete="off" value="ALL" />
                                    <input type="hidden" id="hidtxtAgencyName" name="hidtxtAgencyName" value="" />
                                </div>
                                <div class="col-md-4" id="divPartnerName" runat="server">
                                    <label for="exampleInputEmail1">PartnerName :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPartnerName" runat="server">
                                    </asp:DropDownList>
                                </div>

                                  <div class="col-md-4">

                                <label for="exampleInputPassword1" id="tdTripNonExec1" runat="server">Trip</label>
                                <div id="tdTripNonExec2" runat="server">
                                    <asp:DropDownList ID="ddlTripRefunDomIntl" class="form-control" runat="server">
                                        <asp:ListItem Value="">-----Select-----</asp:ListItem>
                                        <asp:ListItem Value="D">Domestic</asp:ListItem>
                                        <asp:ListItem Value="I">International</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                                <div class="col-md-4">
                                    <label for="exampleInputPassword1" style="visibility: hidden">Status</label>
                                    <div id="tr_ExecID" runat="server">

                                        <div class="large-1 medium-1 small-3 large-push-1 medium-push-1 columns" style="display: none;">
                                        </div>
                                        <div class="large-2 medium-2 small-9 large-push-1 medium-push-1 columns" style="display: none;">
                                            <asp:DropDownList ID="ddl_Status" class="form-control" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group" id="">
                                    <label for="exampleInputPassword1">Ticket No</label>
                                    <asp:TextBox ID="txt_TktNo" runat="server" class="form-control"></asp:TextBox>
                                </div>
                            </div>

                              <div class="col-md-4" id="divPaymentMode" runat="server">
                                    <label for="exampleInputEmail1">PaymentMode :</label>
                                    <asp:DropDownList CssClass="form-control" ID="txtPaymentmode" runat="server">                                 
                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                   <asp:ListItem Text="PG" Value="pg"></asp:ListItem>
                                  <asp:ListItem Text="Wallet" Value="wallet"></asp:ListItem>                                                                
                                    </asp:DropDownList>
                                </div>

                          
                             <div class="col-md-4">
                                <div class="form-group">
                                    <br />
                                    <asp:Button ID="btn_result" runat="server" Text="Search Result" CssClass="button buttonBlue" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                     <br />
                                    <asp:Button ID="btn_export" runat="server" Text="Export" CssClass="button buttonBlue" />
                                </div>
                            </div>

                          
                        </div>

                            <div class="row" id="divReport" style="background-color: #fff; overflow: auto; max-height: 500px;" runat="server" visible="false">
                            <div class="col-md-12">
                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="GridView1_PageIndexChanging"
                                    AutoGenerateColumns="False" CssClass="table " GridLines="None" PageSize="30">
                                    <Columns>
                                       <asp:TemplateField HeaderText="CreatedDate/Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("CreatedDate/Time")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        
                                           <asp:TemplateField HeaderText="TX ID">
                                            <ItemTemplate>

                                                <a id="ancher" href='../PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%>' target="_blank"
                                                    style="font-family: arial, Helvetica, sans-serif; font-size: 12px; color: #004b91; font-weight: bold;" title="click to view">
                                                    <asp:Label ID="lbl_OrderId" runat="server" Text='<%#Eval("OrderId") %>'></asp:Label></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AgencyName">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AgencyName" runat="server" Text='<%#Eval("AgencyName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GDSPNR">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_GdsPnr" runat="server" Text='<%#Eval("GdsPnr")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="AirlinePnr">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_AirlinePnr" runat="server" Text='<%#Eval("AirlinePnr")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Sector">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Sector" runat="server" Text='<%#Eval("sector") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PaxName">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaxName" runat="server" Text='<%#Eval("PaxName")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Carrier">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Carrier" runat="server" Text='<%#Eval("Carrier")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TripType" runat="server" Text='<%#Eval("TripType") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trip">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Trip" runat="server" Text='<%#Eval("Trip") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Booking Amount Gross">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_BookingAmountGross" runat="server" Text='<%#Eval("BookingAmountGross")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingAmountNet">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_BookingAmountNet" runat="server" Text='<%#Eval("BookingAmountNet")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TotalAfterDis" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Payment Mode">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PaymentMode" runat="server" Text='<%#Eval("PaymentMode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SupplierID">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SupplierID" runat="server" Text='<%#Eval("SupplierID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TimeSincePending">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_TimeSincePending" runat="server" Text='<%#Eval("TimeSincePending")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Branch">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_CreateDate" runat="server" Text='<%#Eval("Branch")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SalesExecID">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_PGCharges" runat="server" Text='<%#Eval("SalesExecID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EmptyDataRowStyle CssClass="EmptyRowStyle" />
                                    <PagerStyle CssClass="PagerStyle" />
                                    <SelectedRowStyle CssClass="SelectedRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle"/>
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <AlternatingRowStyle CssClass="AltRowStyle" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  
                <script type="text/javascript">
                    var UrlBase = '<%=ResolveUrl("~/") %>';
                </script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

                <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
</asp:Content>