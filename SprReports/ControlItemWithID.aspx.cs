﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Configuration;

public partial class ControlItemWithID : System.Web.UI.Page
{
    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
    int controlID, ItemID;
    string title = "", subtitle = "", ImageURL = "", LinkedURL = "", STRResult = "", controlName = "";
    decimal price = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                DataTable dt = new DataTable();
                dt = SelectDDL("DDL");
                ddl_controlID.DataSource = dt;
                ddl_controlID.DataTextField = "ControlName";
                ddl_controlID.DataValueField = "ControlID";
                ddl_controlID.DataBind();
                BindData();
            }
        }
        catch (Exception)
        {
        }
    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        try
        {
            string filename = Path.GetFileName(fu_images.PostedFile.FileName);
            fu_images.SaveAs(Server.MapPath("ADImages/" + filename));
            ImageURL = "~/SprReports/ADImages/" + filename;
            controlID = Convert.ToInt32(ddl_controlID.SelectedValue);
            controlName = ddl_controlID.SelectedItem.ToString();
            title = txt_title.Text.ToString();
            subtitle = txt_subtitle.Text.ToString();
            price = Convert.ToDecimal(txt_price.Text.Trim().ToString());
            LinkedURL = txt_linkedurl.Text.ToString();
            STRResult = InsertControlWithItemID(controlID, controlName, title, subtitle, price, ImageURL, LinkedURL, "InsertControlItem", Session["UID"].ToString());
            ClearInputs(Page.Controls);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('" + STRResult + "');", true);
            BindData();
        }
        catch (Exception)
        {
        }
    }
    public void BindData()
    {
        try
        {
            DataTable GVdt = new DataTable();
            GVdt = BindGrid("GridBind", ddl_controlID.SelectedItem.Text.ToString());
            gv1.DataSource = GVdt;
            gv1.DataBind();
        }
        catch (Exception)
        {
        }
    }
    protected void gv1_RowEditing(object sender, GridViewEditEventArgs e)
    {
        gv1.EditIndex = e.NewEditIndex;
        BindData();
    }
    protected void gv1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int index = e.RowIndex;
            DataTable GVdt = new DataTable();
            GVdt = BindGrid("GridBind", ddl_controlID.SelectedItem.Text.ToString());
            string ImageURL = GVdt.Rows[index]["ImageURL"].ToString();
            GridViewRow row = (GridViewRow)gv1.Rows[index];
            Label lbl_item = (Label)row.FindControl("lbl_ItemID");
            TextBox txt_title = (TextBox)row.FindControl("txt_title");
            TextBox txt_subtitle = (TextBox)row.FindControl("txt_subtitle");
            TextBox txt_price = (TextBox)row.FindControl("txt_price");
            TextBox txt_linked = (TextBox)row.FindControl("txt_linked");
            FileUpload fu = (FileUpload)row.FindControl("fu1");
            ItemID = Convert.ToInt32(lbl_item.Text.Trim().ToString());
            title = txt_title.Text.ToString();
            subtitle = txt_subtitle.Text.ToString();
            price = Convert.ToDecimal(txt_price.Text.Trim().ToString());
            LinkedURL = txt_linked.Text.ToString();
            string UpdateFilename = fu.FileName.Trim().ToString();

            if (fu.HasFile)
            {
                string file = System.IO.Path.Combine(Server.MapPath("~/SprReports/ADImages/"), fu.FileName);
                fu.SaveAs(file);
                UpdateFilename = "~/SprReports/ADImages/" + UpdateFilename;
                if (System.IO.File.Exists(UpdateFilename))
                {
                    System.IO.File.Delete(UpdateFilename);
                }
                STRResult = UpdateControlWithItemID(ItemID, title, subtitle, price, UpdateFilename, LinkedURL, "Updated", Session["UID"].ToString());
            }
            else
            {
                STRResult = UpdateControlWithItemID(ItemID, title, subtitle, price, ImageURL, LinkedURL, "Updated", Session["UID"].ToString());
            }
            if (STRResult == "Y")
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc(2);", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc(3);", true);
            }
            gv1.EditIndex = -1;
            BindData();
        }
        catch (Exception)
        {
        }
    }
    protected void gv1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        gv1.EditIndex = -1;
        BindData();
    }
    protected void gv1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        {
            try
            {
                int index = e.RowIndex;
                DataTable GVdt = new DataTable();
                GVdt = BindGrid("GridBind", ddl_controlID.SelectedItem.Text.ToString());
                if (GVdt.Rows.Count > 0)
                {
                    string ImageURL = GVdt.Rows[index]["ImageURL"].ToString();
                    GridViewRow row = gv1.Rows[e.RowIndex];
                    Label lbl_ItemID = (Label)row.FindControl("lbl_ItemID");
                    var path = ImageURL;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    STRResult = DeleteControlWithItemID(Convert.ToInt32(lbl_ItemID.Text.Trim().ToString()), "Deleted");
                    BindData();
                    if (STRResult == "Y")
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc(1);", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc(3);", true);
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }

    protected void Role_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindData();
      
       
    }
    protected void gv1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv1.PageIndex = e.NewPageIndex;
        BindData();
    }
    void ClearInputs(ControlCollection ctrls)
    {
        try
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                ClearInputs(ctrl.Controls);
            }
        }
        catch (Exception ex)
        {
        }
    }
    public DataTable SelectDDL(string Cmd_Type)
    {
        DataTable DTDDL = new DataTable();
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROL", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlName", "");
            cmd.Parameters.AddWithValue("@Width", 0);
            cmd.Parameters.AddWithValue("@Height", 0);
            cmd.Parameters.AddWithValue("@DisplayTime", 0);
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@Cmd_Type", Cmd_Type);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            da.Fill(DTDDL);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return DTDDL;
    }
    public string InsertControlWithItemID(int ControlID, string controlName, string title, string subtitle, decimal price, string ImageURL, string LinkedURL, string CMD_TYPE, string CreatedBy)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ControlID);
            cmd.Parameters.AddWithValue("@controlName", controlName);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@subtitle", subtitle);
            cmd.Parameters.AddWithValue("@price", price);
            cmd.Parameters.AddWithValue("@ImageURL", ImageURL);
            cmd.Parameters.AddWithValue("@LinkedURL", LinkedURL);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return ResultVal;
    }

    //public DataTable BindGrid(string Cmd_Type)
    //{
    //    DataTable DTDDL = new DataTable();
    //    try
    //    {
    //        SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
    //        cmd.CommandType = CommandType.StoredProcedure;
    //        cmd.Parameters.AddWithValue("@ControlID", 0);
    //        cmd.Parameters.AddWithValue("@controlName", "");
    //        cmd.Parameters.AddWithValue("@title", "");
    //        cmd.Parameters.AddWithValue("@subtitle", "");
    //        cmd.Parameters.AddWithValue("@price", 0.00);
    //        cmd.Parameters.AddWithValue("@ImageURL", "");
    //        cmd.Parameters.AddWithValue("@LinkedURL", "");
    //        cmd.Parameters.AddWithValue("@CreatedBy", "");
    //        cmd.Parameters.AddWithValue("@CMD_TYPE", Cmd_Type);
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        if (con.State == ConnectionState.Open)
    //        {
    //            con.Close();
    //        }
    //        else
    //        {
    //            con.Open();
    //        }
    //        da.Fill(DTDDL);
    //        con.Close();
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //    return DTDDL;
    //}

    public DataTable BindGrid(string Cmd_Type,string controlname)
    {
        DataTable DTDDL = new DataTable();
        try
        {
           
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", 0);
            cmd.Parameters.AddWithValue("@controlName", controlname);
            cmd.Parameters.AddWithValue("@title", "");
            cmd.Parameters.AddWithValue("@subtitle", "");
            cmd.Parameters.AddWithValue("@price", 0.00);
            cmd.Parameters.AddWithValue("@ImageURL", "");
            cmd.Parameters.AddWithValue("@LinkedURL", "");
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@CMD_TYPE", Cmd_Type);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            da.Fill(DTDDL);
            con.Close();
        }
        catch (Exception ex)
        {

        }
        return DTDDL;
    }
    public string UpdateControlWithItemID(int ItemID, string title, string subtitle, decimal price, string ImageURL, string LinkedURL, string CMD_TYPE, string CreatedBy)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ItemID);
            cmd.Parameters.AddWithValue("@controlName", "");
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@subtitle", subtitle);
            cmd.Parameters.AddWithValue("@price", price);
            cmd.Parameters.AddWithValue("@ImageURL", ImageURL);
            cmd.Parameters.AddWithValue("@LinkedURL", LinkedURL);
            cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return ResultVal;
    }
    public string DeleteControlWithItemID(int ItemID, string CMD_TYPE)
    {
        string ResultVal = "";
        try
        {
            SqlCommand cmd = new SqlCommand("USP_INSERTCONTROLITEMS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ControlID", ItemID);
            cmd.Parameters.AddWithValue("@controlName", "");
            cmd.Parameters.AddWithValue("@title", "");
            cmd.Parameters.AddWithValue("@subtitle", "");
            cmd.Parameters.AddWithValue("@price", 0.00);
            cmd.Parameters.AddWithValue("@ImageURL", "");
            cmd.Parameters.AddWithValue("@LinkedURL", "");
            cmd.Parameters.AddWithValue("@CreatedBy", "");
            cmd.Parameters.AddWithValue("@CMD_TYPE", CMD_TYPE);
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
            ResultVal = Convert.ToString(cmd.ExecuteScalar());
            con.Close();
        }
        catch (Exception ex)
        {
        }
        return ResultVal;
    }
}