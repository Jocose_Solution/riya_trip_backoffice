﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
public partial class SprReports_Admin_HotelServiceCredentials : System.Web.UI.Page
{  
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["UID"] == null)
            {
                Response.Redirect("~/Login.aspx", true);
            }
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Type"])))
            {
                Response.Redirect("~/Login.aspx");
            }       
            if (!IsPostBack)
            {
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
        }

    }
    public void BindGrid()
    {
        try
        {
            List<HotelShared.HotelCredencials> objhtlcredencials = new List<HotelShared.HotelCredencials>();
            HotelDAL.HotelDA objhtldl = new HotelDAL.HotelDA();
            objhtlcredencials = objhtldl.GETCredencialsList(objhtlcredencials, "ALL");
            grd_HotelSwitchCredencials.DataSource = objhtlcredencials;
            grd_HotelSwitchCredencials.DataBind();
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
        }

    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            HotelDAL.HotelDA objhtldl = new HotelDAL.HotelDA();
            Label lblSNo = (Label)(grd_HotelSwitchCredencials.Rows[e.RowIndex].FindControl("lblId"));
            int i = objhtldl.INS_HotelServiseCredencials("", "", "", "", "", false, "DELETE", Convert.ToInt32(lblSNo.Text));
            BindGrid();
           
            if (i > 0)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully deleted.');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Problen in deleting record.');", true);
            }
        }
        catch (Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
        }
    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string item = e.Row.Cells[0].Text;
            foreach (Button button in e.Row.Cells[2].Controls.OfType<Button>())
            {
                if (button.CommandName == "Delete")
                {
                    button.Attributes["onclick"] = "if(!confirm('Do you want to delete?')){ return false; };";
                }
            }
        }
    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grd_HotelSwitchCredencials.PageIndex = e.NewPageIndex;
        this.BindGrid();
    }


    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (DdlProvider.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select provider');", true);
                return;
            }
            else
            {
                #region Insert
                HotelDAL.HotelDA objhtldl = new HotelDAL.HotelDA();
                string Provider = DdlProvider.SelectedValue;
                string TripType = DdlTripType.SelectedValue;
                bool   DdlStat= Convert.ToBoolean( DdlStatus.SelectedValue);
                int i = objhtldl.INS_HotelServiseCredencials(Provider, TripType, TxtUserID.Text, TxtPassword.Text, TxtCorporateID.Text, DdlStat, "INSERT", 0);
               // int flag = InsertServiceCredential(CorporateID, UserID, Password, LoginID, LoginPwd, ServerUrlOrIP, BkgServerUrlOrIP, Port, Provider, CarrierAcc, SearchType, Status, ResultFrom, CrdType, TripType, TripTypeName, AirlineCode, AirlineName);
                if (i > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Added successfully.!!');", true);
                    ClearValue();
                    BindGrid();
                }
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('try again.');window.location='HotelServiceCredentials.aspx'; ", true);
                #endregion
            }
        }
        catch (Exception ex)
        {
            clsErrorLog.LogInfo(ex);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('" + ex.Message + "');window.location='HotelServiceCredentials.aspx'; ", true);
        }

    }

    public void ClearValue()
    {
        TxtCorporateID.Text = "";
        TxtUserID.Text = "";
        TxtPassword.Text = "";
        DdlProvider.SelectedValue = "0";
    }

    protected void grd_HotelSwitchCredencials_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        grd_HotelSwitchCredencials.EditIndex = -1;
        BindGrid();
    }
    protected void grd_HotelSwitchCredencials_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
             HotelDAL.HotelDA objhtldl = new HotelDAL.HotelDA();
             var ddl = grd_HotelSwitchCredencials.Rows[e.RowIndex].FindControl("grdDdlStatus") as DropDownList;
             string text = ddl.SelectedItem.Text;
             string value = ddl.SelectedItem.Value;
           // DropDownList ddlEmployee = grd_HotelSwitchCredencials.Rows(<grd_HotelSwitchCredencials.EditIndex>).FindControls("grdDdlStatus") as DropDownList;
             Label lblSNo = (Label)(grd_HotelSwitchCredencials.Rows[e.RowIndex].FindControl("lblId"));
            Label Counter = (Label)(grd_HotelSwitchCredencials.Rows[e.RowIndex].FindControl("lblId"));
            int i = objhtldl.INS_HotelServiseCredencials("", "", "", "", "",  Convert.ToBoolean(ddl.SelectedItem.Value), "UPDATE", Convert.ToInt32(lblSNo.Text));
            grd_HotelSwitchCredencials.EditIndex = -1;
            BindGrid();
            if (i > 0)
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' Record successfully Updated.');", true);
             else
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Alert", "alert(' This Record is Already In Database. Please Insert Another Record.');", true);            
        }
        catch(Exception ex)
        {
            HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
        }
    }
    protected void grd_HotelSwitchCredencials_RowEditing(object sender, GridViewEditEventArgs e)
    {
          grd_HotelSwitchCredencials.EditIndex = e.NewEditIndex;
          BindGrid();
    }
}