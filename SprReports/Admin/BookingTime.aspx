﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BookingTime.aspx.vb" Inherits="SprReports_BookingTime" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script language="javascript" type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if ((charCode >= 48 && charCode <= 57) || (charCode == 08 || charCode == 46 || charCode == 32)) {
                return true;
            }
            else {
                return false;
            }
        }

        function validateptk() {
            var ddltype = document.getElementById("typedd");
            if (ddltype.value == 0) {
                //If the "Please Select" option is selected display error.
                alert("Please select Status Before Processing To Hold PNR!");
                return false;
            }
        }

    </script>
     <link href="../../CSS/foundation.min.css" rel="stylesheet" />
    <link href="../../CSS/foundation.css" rel="stylesheet" />
    <link href="../../CSS/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/style.css" rel="stylesheet" type="text/css" />

    <style>
        input[type="text"], input[type="password"], select, textarea {
            border: 1px solid #808080;
            padding: 2px;
            font-size: 1em;
            color: #444;
            width: 150px;
            font-family: arial, Helvetica, sans-serif;
            font-size: 12px;
            font-weight: normal;
            border-radius: 3px 3px 3px 3px;
            -webkit-border-radius: 3px 3px 3px 3px;
            -moz-border-radius: 3px 3px 3px 3px;
            -o-border-radius: 3px 3px 3px 3px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>


             <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Booking Time
                    </td>
                </tr>
                <tr>
                    <td>                         
                                <asp:GridView ID="GDBookingtime" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>
                                          <asp:TemplateField HeaderText="Create/Request Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRequest" runat="server" Text='<%#Eval("Request")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRequest" Width="80px" runat="server" Text='<%#Eval("Request")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Accept Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccept" runat="server" Text='<%#Eval("Accept")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAccept" Width="80px" runat="server" Text='<%#Eval("Accept")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Update Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUpdate" runat="server" Text='<%#Eval("Update")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtUpdate" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("Update")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField> 
                                            <asp:BoundField HeaderText="PnrNo" DataField="PnrNo"></asp:BoundField>
                                         <asp:BoundField HeaderText="SearchId" DataField="SearchId"></asp:BoundField>
                                            <asp:BoundField HeaderText="BookingId" DataField="PNRId"></asp:BoundField>
                                            <asp:BoundField HeaderText="TicketId" DataField="TicketId"></asp:BoundField>
                                          <asp:BoundField HeaderText="ExecutiveID" DataField="ExecutiveID"></asp:BoundField>                                     
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                    </td>
                </tr>

            </table>

            
               <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">LedgerDetails
                    </td>
                </tr>
                <tr>
                    <td>
                        
                         
                                <asp:GridView ID="Grd_LedgerDetails" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="GridViewStyle">
                                    <Columns>

                                          <asp:TemplateField HeaderText="IPAddress">
                                            <ItemTemplate>
                                                <asp:Label ID="lblIPAddress" runat="server" Text='<%#Eval("IPAddress")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtIPAddress" Width="80px" runat="server" Text='<%#Eval("IPAddress")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="PnrNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPnrNo" runat="server" Text='<%#Eval("PnrNo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtPnrNo" Width="80px" runat="server" Text='<%#Eval("PnrNo")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TicketNo">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketNo" runat="server" Text='<%#Eval("TicketNo")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTicketNo" Width="70px" MaxLength="3" runat="server" Text='<%#Eval("TicketNo")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TicketingCarrier">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTicketingCarrier" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtTicketingCarrier" Width="80px" runat="server" Text='<%#Eval("TicketingCarrier")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ExecutiveID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblExecutiveID" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtExecutiveID" MaxLength="3" Width="70px" runat="server" Text='<%#Eval("ExecutiveID")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Debit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDebit" runat="server" Text='<%#Eval("Debit")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDebit" Width="80px" runat="server" Text='<%#Eval("Debit")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Credit">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCredit" runat="server" Text='<%#Eval("Credit")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtCredit" Width="60px" runat="server" Text='<%#Eval("Credit")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Aval_Balance">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAval_Balance" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtAval_Balance" Width="50px" runat="server" Text='<%#Eval("Aval_Balance")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BookingType">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBookingType" runat="server" Text='<%#Eval("BookingType")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtBookingType" MaxLength="6" onkeypress="return isNumberKey(event)" Width="60px" runat="server" Text='<%#Eval("BookingType")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remark">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRemark" runat="server" Text='<%#Eval("Remark")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtRemark" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("Remark")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DISTRID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDISTRID" runat="server" Text='<%#Eval("DISTRID")%>'></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtDISTRID" onkeypress="return isNumberKey(event)" Width="50px" runat="server" Text='<%#Eval("DISTRID")%>'></asp:TextBox>
                                            </EditItemTemplate>
                                        </asp:TemplateField>
                                        <%-- <asp:TemplateField HeaderText="Edit/Update">
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" Text="Edit" CommandName="Edit" CssClass="newbutton_2"/>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update" CssClass="newbutton_2"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CssClass="newbutton_2"/>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>
                                    </Columns>
                                    <HeaderStyle CssClass="HeaderStyle" />
                                </asp:GridView>
                                <b>
                                    <asp:Label ID="Label1" runat="server" Visible="false"></asp:Label></b>
                        
               
                    </td>
                </tr>

            </table>

          <div>
                &nbsp;
            </div>
            <table width="90%" class="tbltbl" border="0" cellpadding="10" cellspacing="10">
                <tr>
                    <td align="left" style="font-weight: bold; font-size: 14px;">Fare Information
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFareInformation" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
