﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CancelPaxDetails.aspx.vb" Inherits="SprReports_RefundMass_CancelPaxDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
     <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-12">
            <div class="page-wrapperss">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Pax Details For Cancellation</h3>
                    </div>
                    <div class="panel-body">

                        <asp:GridView ID="PAXDETAILS" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
                            <Columns>                             
                             <asp:BoundField HeaderText="PaxName" DataField="paxname"></asp:BoundField>
                            <asp:BoundField HeaderText="paxType" DataField="pax_type"></asp:BoundField>
                            <asp:BoundField HeaderText="Tkt_No" DataField="Tkt_No"></asp:BoundField>
                            <asp:BoundField HeaderText="Sector" DataField="Sector"></asp:BoundField>
                             <asp:BoundField HeaderText="TotalFare" DataField="TotalFare"></asp:BoundField>
                            <asp:BoundField HeaderText="NetFare" DataField="TotalFareAfterDiscount"></asp:BoundField>
                            <asp:BoundField HeaderText="BookingDate" DataField="Booking_date"></asp:BoundField>
                           </Columns>

                            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
                            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F7F7F7" />
                            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
                            <SortedDescendingCellStyle BackColor="#E5E5E5" />
                            <SortedDescendingHeaderStyle BackColor="#242121" />

                        </asp:GridView>
                          </div></div></div></div>
    </div>
    </form>
</body>
</html>
