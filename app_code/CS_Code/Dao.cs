﻿using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Collections;
using System.Data.SqlClient;



public class Dao
{


    public string  Roleinsert(Pro obj)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();


        try
        {
            SqlCommand cmd = new SqlCommand("RoleSP_PP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Role", obj.U_role);
            cmd.Parameters.AddWithValue("@Role_Type", obj.Role_Type);

            string ret = "";
            ret=cmd.ExecuteScalar().ToString();
            return ret;
                

        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }

    public string Pageinsert(Pro obj)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();



        try
        {

            SqlCommand cmd = new SqlCommand("USP_PAGEID_PP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Page_name", obj.Page_name);
            cmd.Parameters.AddWithValue("@Page_url", obj.Page_url);
            cmd.Parameters.AddWithValue("@Rootpageid", obj.Page_id);
            cmd.Parameters.AddWithValue("@IsParent_page", obj.CheckBox);

            //return cmd.ExecuteNonQuery();
            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            return ret;


        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }
    public string User_idinsert(Pro obj)
    {

        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();


        try
        {
            SqlCommand cmd = new SqlCommand("rolesp2_PP", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user_id", obj.U_type);
            cmd.Parameters.AddWithValue("@role", obj.U_role);
            //return cmd.ExecuteNonQuery();
            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            return ret;


        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }
     public  string InsertPage_Role(Pro obj)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        con.Open();


        try
        {
            SqlCommand cmd = new SqlCommand("RoleInsertSp1_PP", con);
        
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@role_id", obj.Role_id);
            cmd.Parameters.AddWithValue("@page_id", obj.Page_id);

            string ret = "";
            ret = cmd.ExecuteScalar().ToString();
            return ret;

        }
        catch (Exception info)
        {
            throw info;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }


    }



     public string insertdata(Pro obj,string Branch)
     {
         SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
         con.Open();


         try
         {
             SqlCommand cmd = new SqlCommand("BindRoleNewExecutive_PP", con);

             cmd.CommandType = CommandType.StoredProcedure;
             cmd.Parameters.AddWithValue("@userid", obj.user_id);
             cmd.Parameters.AddWithValue("@password", obj.password);
             cmd.Parameters.AddWithValue("@role_id", obj.Role_id);
            // cmd.Parameters.AddWithValue("@role_type", obj.Role_Type);
             cmd.Parameters.AddWithValue("@name", obj.name);
             cmd.Parameters.AddWithValue("@email_id", obj.email);
             cmd.Parameters.AddWithValue("@mobile_no", obj.mobileno);
             cmd.Parameters.AddWithValue("@status", obj.status);
             cmd.Parameters.AddWithValue("@Branch", Branch);
             string ret = "";
             ret = cmd.ExecuteScalar().ToString();
             return ret;

         }
         catch (Exception info)
         {
             throw info;
         }
         finally
         {
             con.Close();
             con.Dispose();
         }


     }
    }



    